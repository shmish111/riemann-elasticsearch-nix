{ nixpkgs ? <nixpkgs>
}:
let pkgs = import nixpkgs {};
in
{
  riemann-elasticsearch = pkgs.callPackage ../default.nix {};
}
