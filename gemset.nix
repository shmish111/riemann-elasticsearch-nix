{
  beefcake = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "10gid0a7pdllh3qmjiqkqxgfqvd7m1f2dmcm4gcd19s63pv620gv";
      type = "gem";
    };
    version = "1.0.0";
  };
  faraday = {
    dependencies = ["multipart-post"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "14lg0c4bphk16rccc5jmaan6nfcvmy0caiahpc61f9zfwpsj7ymg";
      type = "gem";
    };
    version = "0.15.2";
  };
  json = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0qmj7fypgb9vag723w1a49qihxrcf5shzars106ynw2zk352gbv5";
      type = "gem";
    };
    version = "1.8.6";
  };
  mtrc = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0xj2pv4cpn0ad1xw38sinsxfzwhgqs6ff18hw0cwz5xmsf3zqmiz";
      type = "gem";
    };
    version = "0.0.4";
  };
  multipart-post = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "09k0b3cybqilk1gwrwwain95rdypixb2q9w65gd44gfzsd84xi1x";
      type = "gem";
    };
    version = "2.0.0";
  };
  riemann-client = {
    dependencies = ["beefcake" "mtrc" "trollop"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "02rp8x2y8h61x8mx9c8kwgm2yyvgg63g8km93zmwmkpp5fyi3fi8";
      type = "gem";
    };
    version = "0.2.6";
  };
  riemann-elasticsearch = {
    dependencies = ["faraday" "json" "riemann-tools"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1y6kj80dx4k7gshnacfhybh5r676l0bfdprp82ic1ndinbxz83sv";
      type = "gem";
    };
    version = "0.2.4";
  };
  riemann-tools = {
    dependencies = ["json" "riemann-client" "trollop"];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0brf44cq4xz0nqhs189zlg76527bfv3jr453yc00410qdzz8fpxa";
      type = "gem";
    };
    version = "0.2.13";
  };
  trollop = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "1rzx9rkacpq58dsvbbzs4cpybls1v1h36xskkfs5q2askpdr00wq";
      type = "gem";
    };
    version = "2.1.3";
  };
}