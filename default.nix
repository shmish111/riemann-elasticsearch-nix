{ bundlerEnv, ruby, stdenv, makeWrapper, xsel }:

stdenv.mkDerivation rec {
  name = "riemann-elasticsearch-0.2.4";
  env = bundlerEnv {
      name = "riemann-elasticsearch-0.2.4";
      ruby = ruby;
      gemfile = ./Gemfile;
      lockfile = ./Gemfile.lock;
      gemset = ./gemset.nix;
      };

  buildInputs = [ makeWrapper ];

  phases = ["installPhase"];

  installPhase = ''
    mkdir -p $out/bin
    makeWrapper ${env}/bin/riemann-elasticsearch $out/bin/riemann-elasticsearch \
      --set PATH '"${xsel}/bin/:$PATH"'
  '';
}
